# alta3research-mycode-cert

## Name
Alta3 Research Python Course Certification

## Description
This project is used to demonstrate my understanding of Flask and requests in Python library.

## Installation
1) Open the terminal / command prompt from your machine
2) Change the directory of the terminal to where you want to store this project
3) Clone this project using one of the following command 
    - git clone git@gitlab.com:karl_choo_jia_long/alta3research-mycode-cert.git (SSH)
    - git clone https://gitlab.com/karl_choo_jia_long/alta3research-mycode-cert.git (HTTPS)
4) Change the directory to the cloned project
5) Install the necessary packages using the command - pip install -r requirements.txt
6) Execute the file alta3research-flask01.py using Python to test the Flask API
7) Execute the file alta3research-requests02.py using Python to run the Pokemon Berry Query CLI application

## Usage
Use to test Flask and requests library for Python
The Flask API uses data from GitLab to show data related to Arknights, a mobile game
The requests library was use to create a CLI application related to Pokemon Berries via PokeAPI

## Support
karl_jia_long_choo@dell.com - Karl Jia Long Choo (Developer)

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
This project uses GNU General Public License v3.0

## Project status
This project will not be developed any further as it just used for testing.
