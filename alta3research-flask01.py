#!/usr/bin/env python3
''' 
    Alta 3 Certification for Flask test - Arknights Operator API application'''

from datetime import datetime
from operator import attrgetter
import json
import requests

from flask import Flask
from flask import request
from flask import render_template
from flask import redirect
from flask import url_for


app = Flask(__name__)

# Test if the API is online
@app.route('/', methods=["GET"])
def index():
    return {"msg": "API is working."}

# Display the list of operators in arknights. Can use filters
@app.route('/arknights/operators', methods=["GET"])
def arknights_chars():
    try:
        qparms = {}
        qparms["class"] = request.args.get("class", "") # The class of the operator.
        qparms["subclass"] = request.args.get("subclass", "") # The subclass of the operator.
        qparms["position"] = request.args.get("position", "") # The position of the operator (MELEE or RAANGED)
        qparms["rarity"] = request.args.get("rarity", "") # The rarity of the operator (1 - 6 stars)
        qparms["format"] = request.args.get("format", "") # The format of the data returned. json is the only valid format for now.
        # Retrieves the arknights operators data from file
        with open("arknights_char.json", "r") as f:
            arknights_char_data = json.load(f)
            # Remove list of NPC from the list
            arknights_char_data["results"] = list(filter(lambda x: not "notchar" in x["subProfessionId"] and x["profession"] != "TOKEN" , arknights_char_data["results"]))
            arknights_char_data["results"] = sorted(arknights_char_data["results"], key=lambda x: (x["rarity"] * -1, x["name"]))
            
            # Filter by class, can use the /arknights/class to view list of class
            if qparms["class"].strip() != "":
                arknights_char_data["results"] = list(filter(lambda x: x['profession'] == qparms["class"].strip().upper(), arknights_char_data["results"]))

            # Filter by subclass, can use the /arknights/class to view list of subclass
            if qparms["subclass"].strip() != "":
                arknights_char_data["results"] = list(filter(lambda x: x['subProfessionId'] == qparms["subclass"].strip().lower(), arknights_char_data["results"]))

            # Filter by rarity, 1 - 6 are the only valid values
            if qparms["rarity"].strip() in ["1","2","3","4","5","6"]:
                arknights_char_data["results"] = list(filter(lambda x: x['rarity'] == int(qparms["rarity"]) - 1, arknights_char_data["results"]))

            # Filter by operator position
            if qparms["position"].strip().upper() in ["MELEE", "RANGED"]:
                arknights_char_data["results"] = list(filter(lambda x: x['position'] == qparms["position"].upper(), arknights_char_data["results"]))

            # Reset the operator count after the filter
            arknights_char_data["operator_count"] = len(arknights_char_data["results"])

            # Choose to return the format of data, json has more details
            if qparms["format"] == "json":
                return arknights_char_data
            else:
                return render_template("arknights_operators.html", arknights_char_data=arknights_char_data)

    except Exception as e:
        print(e)
        return {"msg": "Failed to retreive file."}

# Displays the list of classes and its list of subclasses
@app.route('/arknights/class', methods=["GET"])
def arknights_class():
    try:
        qparms = {} 
        qparms["format"] = request.args.get("format", "") # The format of the data returned. json is the only valid format for now.

        # Retrieves the arknights operators class from file
        with open("arknights_class.json", "r") as f:
            arknights_class_data = json.load(f)

            # Choose to return the format of data, json has more details
            if qparms["format"] == "json":
                return arknights_class_data
            else:
                return render_template("arknights_class.html", arknights_class_data=arknights_class_data)
    except Exception as e:
        print(e)
        return {"msg": "Failed to retreive file."}

# Updates the list of operators from the data source before saving it to a json file
@app.route('/arknights/operators/update', methods=["GET"])
def arknights_update():

    # Parses id into the dictionary which was originally the key for the values of operator data
    def parse_data(x, arknights_char_data):
        # temp dictionary and update function is used to ensure the id key is at the top for each operator record
        temp = {}
        temp["id"] = x
        temp.update(arknights_char_data[x])
        return temp

    try:

        qparms = {}
        # This key is used to redirect to operator page regardless of value, can be useful to create refresh buttons or other use cases
        qparms["r"] = request.args.get("r", None) 

        # Data source from GitHub about Arknights operators
        response = requests.get('https://raw.githubusercontent.com/Kengxxiao/ArknightsGameData/master/en_US/gamedata/excel/character_table.json')
        arknights_char_data = response.json()
        arknights_char_data_list = list(map(lambda x: parse_data(x,arknights_char_data),arknights_char_data))

        save_obj = {} # Save some metadata along with the list of operators
        with open("arknights_char.json", "w") as f:
            save_obj["refreshed_date"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            save_obj["operator_count"] = len(arknights_char_data)
            save_obj["results"] = arknights_char_data_list
            f.write(json.dumps(save_obj, indent=2))
            if not qparms["r"] is None:
                return redirect(url_for("arknights_chars"))
            return {"msg": "Data successfully updated."}
    except Exception as e:
        print(e)
        print('Failed to load data from source.')
        return {"msg": "Failed to load data from source."}

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=2224)