#!usr/bin/env python3
''' 
    Alta 3 Certification for requests library Python - Pokemon Berry CLI application'''

import time
import requests

def main():
    
    # function for option 1, fetching all berry from PokeAPI
    def get_all_bery_name_and_id():
        pokemon_berry_url = "https://pokeapi.co/api/v2/berry/?limit=2000" # There are only 900+ pokemon species as of November 2022
        pokemon_berry_data = {}
        try:
            response = requests.get(pokemon_berry_url)
            if response.status_code == 200:
                pokemon_berry_data = response.json()
        except requests.exceptions.RequestException as e:
            print(e)
            return {"msg": "Failed to retrieve data from Pokeapi."}
        except Exception as e:
            print(type(e))
            print(e)
            return {"msg": "Failed to retrieve data from Pokeapi."}
        
        return pokemon_berry_data

    # function for option 2, fetching detail off a berry by name or id
    def get_berry_detail(berry_id_or_name):
        pokemon_berry_url = f"https://pokeapi.co/api/v2/berry/{berry_id_or_name}"
        pokemon_berry_detail = {}
        try:
            response = requests.get(pokemon_berry_url)
            if response.status_code == 200:
                pokemon_berry_detail = response.json()
            if response.status_code == 404:
                return {"msg": "That berry does not exist."}
        except requests.exceptions.RequestException as e:
            print(e)
            return {"msg": "Failed to retrieve data from Pokeapi."}
        except Exception as e:
            print(type(e))
            print(e)
            return {"msg": "Failed to retrieve data from Pokeapi."}

        return pokemon_berry_detail
            

    # Infinite loop menu
    while True:

        ## display menu
        print("Pokemon Berry Query")
        print("---------------------------------")
        print("1) Show all berry names and id")
        print("2) Get berry details")
        print("99) Exit")

        ## get value from user, empty
        userinput = ""
        while userinput == "":
            userinput = input("> ")
        userinput = userinput.strip()
        if userinput == "1":
            berry_data = get_all_bery_name_and_id()
            if "msg" in berry_data:
                print(berry_data["msg"])
            else:
                print(f"There are total of {berry_data['count']} berries currently.")
                berry_list = berry_data["results"]
                for index, berry in enumerate(berry_list):
                    print(f"({index + 1}) {berry['name'].capitalize()} [{berry['url']}]")
        elif userinput == "2":
            berry_id_or_name = ""
            while berry_id_or_name == "":
                berry_id_or_name = input("Enter the berry id or name. ")
            berry_id_or_name = berry_id_or_name.lower().strip()
            berry_detail = get_berry_detail(berry_id_or_name)
            if "msg" in berry_detail:
                print(berry_detail["msg"])
            else:
                print(f"ID: {berry_detail['id']}")
                print(f"Name: {berry_detail['name'].capitalize()}")
                print(f"Size (mm): {berry_detail['size']}")
                print(f"Flavor(s) & Potency: {', '.join(list(map(lambda x : x['flavor']['name'] + ' (' + str(x['potency']) + ')',berry_detail['flavors'])))}")
                print(f"Firmness: {berry_detail['firmness']['name'].capitalize()}")
                print(f"Growth Time: {berry_detail['growth_time']} hours per stage")
                print(f"Max Harvest: {berry_detail['max_harvest']} per tree")
                print(f"Smoothness: {berry_detail['smoothness']}")
                print(f"Soil Dryness: {berry_detail['soil_dryness']}")

        elif userinput == "99":
            print("Thanks for using Pokemon Berry Query.")
            time.sleep(1)
            quit()
        else:
            print("Invalid input.\n")


if __name__ == "__main__":
    main()